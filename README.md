# thunderbird-webui

![](https://img.shields.io/badge/written%20in-Javascript%20%28XPCOM%29-blue)

A web-based interface for Mozilla Thunderbird.

Supports reading email - never extended to do much else, and i've lost the original blurb.

Usage: Install the .xpi and browse to port 8082.
Source code: Unzip the .xpi

Original publication: https://addons.mozilla.org/en-US/thunderbird/addon/thunderbird-webui/ (now removed)


## Download

- [⬇️ thunderbirdwebui-0.1+tb.xpi](dist-archive/thunderbirdwebui-0.1%2Btb.xpi) *(9.50 KiB)*
